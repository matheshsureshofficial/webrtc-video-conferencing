const express = require("express")
const path = require('path')
const app = express()
const port = process.env.PORT || 4000
const server = app.listen(port, () => { console.log(`App Runnig On http://localhost:${port}`) })
const io = require("socket.io")(server)

app.set("view engine", "ejs")

app.use("/views", express.static(path.join(__dirname + "/views")))
app.use("/public", express.static(path.join(__dirname + "/public")))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))


app.get("/", (req, res) => {
    res.render("index")
})

app.get("/:roomid", (req, res) => {
    res.render("video", { roomid: req.params.roomid })
})

io.on('connection', onConnected)

function onConnected(socket) {
    console.log('Socket connected', socket.id)

    socket.on('join-room', (roomId, userId) => {
        console.log({ "roomId": roomId, "userId": userId })
        socket.join(roomId)        
        socket.to(roomId).emit("user-connected", userId)    
        
        
        socket.on('disconnect', () => {
            socket.to(roomId).emit('user-disconnected', userId)
        })
    })

    socket.on('disconnect', () => {
        console.log('Socket disconnected', socket.id)
    })
}
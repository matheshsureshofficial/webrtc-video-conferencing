var socket = io()
console.log("Roomid: " + roomid)

var peer = new Peer();
const newpeers = {}
peer.on('open', function (userid) {
    console.log("userid: " + userid)
    socket.emit('join-room', roomid, userid)
});

var videoContainer = document.getElementById("video-container")
var myVideo = document.createElement("video")
myVideo.muted = true

navigator.mediaDevices.getUserMedia({
    video: true,
    audio: true
}).then((stream) => {
    displayVideo(myVideo, stream)

    peer.on('call', function (call) {
        call.answer(stream);
        const video = document.createElement('video')
        call.on('stream', function (userVideo) {
            console.log(userVideo,"userVideo")
            displayVideo(video, userVideo)
        });
    });
    socket.on('user-connected', incomeuserId => {
        createnewUservideo(incomeuserId, stream)
    })
})

socket.on('user-disconnected', userId => {
    console.log("user-disconnected:" + userId)
    console.log(newpeers[userId])
    if (newpeers[userId]) {
        newpeers[userId].close()
    }
})

function createnewUservideo(incomeuserId, stream) {
    var call = peer.call(incomeuserId, stream);
    const video = document.createElement('video')
    call.on('stream', function (incomeuserstream) {
        displayVideo(video, incomeuserstream)
    });
    call.on('close', () => {
        video.remove()
    })
    newpeers[incomeuserId] = call
}

function displayVideo(video, stream) {
    video.srcObject = stream
    video.addEventListener('loadedmetadata', () => {
        video.play()
    })

    videoContainer.append(video)
}